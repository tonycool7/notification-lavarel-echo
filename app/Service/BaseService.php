<?php

namespace App\Service;

use Illuminate\Database\Eloquent\Model;

Abstract class BaseService
{
    public $model;

   public function __construct(Model $model)
   {
       $this->model = $model;
   }

}
