<?php

namespace App\Service;

use App\Models\News;
use Illuminate\Http\Request;

class NewsService extends BaseService
{
    public function __construct(News $model)
    {
        parent::__construct($model);
    }

    public function create(Request $request){
        //Validation code here
        //....

        $payload = $request->all();

        $response = null;

        try{
            $response = $this->model->create($payload);
        }catch (\Exception $e){
            dd($e);
        }

        return $response;
    }
}
