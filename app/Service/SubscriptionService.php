<?php


namespace App\Service;


use App\Models\Subscription;
use Illuminate\Http\Request;

class SubscriptionService extends BaseService
{
    public function __construct(Subscription $model)
    {
        parent::__construct($model);
    }

    public function create(Request $request){
        //Validation code here
        //....

        $payload = $request->all();

        $response = null;

        //тут можно по лушче
        $payload1['user_id'] = $request->user()->id;
        $payload1['type'] =  "product";

        $payload2['user_id'] = $request->user()->id;
        $payload2['type'] = "news";

        try{

            if($payload['product'] && !$this->model->where($payload1)->exists()){
                $response = $this->model->create($payload1);
            }else if(!$payload['product'] && $this->model->where($payload1)->exists()){
                $response = $this->model
                    ->where($payload1)->delete();
            }

            if($payload['news'] && !$this->model->where($payload2)->exists()){
                $response = $this->model->create($payload2);
            }else if(!$payload['news'] && $this->model->where($payload2)->exists()){
                $response = $this->model->where($payload2)->first()->delete();
            }
        }catch (\Exception $e){
            dd($e);
        }

        return $response;
    }
}
