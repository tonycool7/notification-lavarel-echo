<?php

namespace App\Service;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductService extends BaseService
{

    public function __construct(Product $model)
    {
        parent::__construct($model);
    }

    public function create(Request $request){
        //Validation code here
        //....

        $payload = $request->all();

        $response = null;

        try{
            $response = $this->model->create($payload);
        }catch (\Exception $e){
            dd($e);
        }

        return $response;
    }

}
